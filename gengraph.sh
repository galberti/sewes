#!/bin/bash

function set_variables() {
    # times
    n=$(date +%s)           # now
    h=$((n-(60*60*4)))      # last 4 hours
    d=$((n-(60*60*24)))     # last day
    w=$((n-(60*60*24*7)))   # last week
    m=$((n-(60*60*24*90)))  # last 3 months
    y=$((n-(60*60*24*365))) # last year (big)

    # intervals
    hi=60          # 1 min
    di=$((5*60))   # 5 mins
    wi=$((di*6))   # 30 mins
    mi=$((wi*4*3)) # 6 hours
    yi=$((mi*4))   # 24 hours
}

function gen_temp() {
    rrdtool graph www/temp-$1.png \
        -t "Last $1 Temperature" -w 960 -h 240 \
        -s $2 \
        -S $3 \
        DEF:Temp=temp.rrd:Temperature:AVERAGE \
        LINE:Temp#FF0000
}

function gen_cpu() {
    rrdtool graph www/cpu-$1.png \
        -t "Last $1 CPU" -w 960 -h 240 \
        -l 0 -u 100 \
        -s $2 \
        DEF:User=cpu.rrd:User:AVERAGE \
        DEF:Nice=cpu.rrd:Nice:AVERAGE \
        DEF:System=cpu.rrd:System:AVERAGE \
        DEF:Idle=cpu.rrd:Idle:AVERAGE \
        CDEF:Tot=User,Nice,System,Idle,+,+,+ \
        CDEF:U=User,Tot,/,100,\* \
        CDEF:N=Nice,Tot,/,100,\* \
        CDEF:S=System,Tot,/,100,\* \
        CDEF:I=Idle,Tot,/,100,\* \
        AREA:S#AAAAFF:System \
        AREA:U#FF0000:User:STACK \
        AREA:N#00FF00:Nice:STACK \
        AREA:I#aaaaaa:Idle:STACK
}

function gen_net() {
    rrdtool graph www/net-$1-$2.png \
        -t "Last $2 Network ($4 secs intervals)" -w 960 -h 240 \
        -l 0.1 \
        -o \
        -s $3 \
        -S $4 \
        DEF:bandin=$1.rrd:bandin:AVERAGE \
        DEF:bandout=$1.rrd:bandout:AVERAGE \
        CDEF:bink=bandin,1024,/ \
        CDEF:boutk=bandout,1024,/ \
        COMMENT:$1 \
        AREA:bink#00aa00:KBIn \
        LINE:boutk#0000aa:KBOut
}

while [ $? -eq 0 ]
do
    set_variables

    gen_temp 4hours $h $hi
    gen_temp day $d $di
    gen_temp week $w $wi
    gen_temp 3months $m $mi
    gen_temp year $y $yi

    gen_cpu 4hours $h
    gen_cpu day $d
    gen_cpu week $w
    gen_cpu 3months $m
    gen_cpu year $y

    gen_net eth0 4hours $h $hi
    gen_net eth0 day $d $di
    gen_net eth0 week $w $wi
    gen_net eth0 3months $m $mi
    gen_net eth0 year $y $yi

    gen_net wlan0 4hours $h $hi
    gen_net wlan0 day $d $di
    gen_net wlan0 week $w $wi
    gen_net wlan0 3months $m $mi
    gen_net wlan0 year $y $yi

    gen_net ap0 4hours $h $hi
    gen_net ap0 day $d $di
    gen_net ap0 week $w $wi
    gen_net ap0 3months $m $mi
    gen_net ap0 year $y $yi

    sleep $((60*5))
done

