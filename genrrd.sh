#!/bin/bash

if [ ! -f temp.rrd ]; then
    # after 450 secs is unknown
    # 1m for 24h, 5m for 30 days, 1h for 90 days, 6h for 2y
    rrdtool create temp.rrd -s 60 \
        DS:Temperature:GAUGE:90:U:U \
        RRA:AVERAGE:0.5:1:24h \
        RRA:AVERAGE:0.5:5:30d \
        RRA:AVERAGE:0.5:60:90d \
        RRA:AVERAGE:0.5:360:2y
fi

if [ ! -f cpu.rrd ]; then
    # 1m for 24h, 5m for 30 days, 1h for 90 days, 6h for 2y
    rrdtool create cpu.rrd -s 60 \
        DS:User:COUNTER:90:U:U \
        DS:Nice:COUNTER:90:U:U \
        DS:System:COUNTER:90:U:U \
        DS:Idle:COUNTER:90:U:U \
        RRA:AVERAGE:0.5:1:24h \
        RRA:AVERAGE:0.5:5:30d \
        RRA:AVERAGE:0.5:60:90d \
        RRA:AVERAGE:0.5:360:2y
fi

if [ ! -f eth0.rrd ]; then
    # 1m for 24h, 5m for 30 days, 1h for 90 days, 6h for 2y
    rrdtool create eth0.rrd -s 60 \
        DS:bandin:COUNTER:90:U:U \
        DS:bandout:COUNTER:90:U:U \
        RRA:AVERAGE:0.5:1:24h \
        RRA:AVERAGE:0.5:5:30d \
        RRA:AVERAGE:0.5:60:90d \
        RRA:AVERAGE:0.5:360:2y
fi

if [ ! -f wlan0.rrd ]; then
    # 1m for 24h, 5m for 30 days, 1h for 90 days, 6h for 2y
    rrdtool create wlan0.rrd -s 60 \
        DS:bandin:COUNTER:90:U:U \
        DS:bandout:COUNTER:90:U:U \
        RRA:AVERAGE:0.5:1:24h \
        RRA:AVERAGE:0.5:5:30d \
        RRA:AVERAGE:0.5:60:90d \
        RRA:AVERAGE:0.5:360:2y
fi

if [ ! -f ap0.rrd ]; then
    rrdtool create ap0.rrd -s 60 \
        DS:bandin:COUNTER:90:U:U \
        DS:bandout:COUNTER:90:U:U \
        RRA:AVERAGE:0.5:1:24h \
        RRA:AVERAGE:0.5:5:30d \
        RRA:AVERAGE:0.5:60:90d \
        RRA:AVERAGE:0.5:360:2y
fi

