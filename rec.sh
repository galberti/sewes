#!/bin/bash

while [ $? -eq 0 ]
do
    date
    T=$(vcgencmd measure_temp | sed -n "s/temp=\([0-9.]*\).*/\1/p")
    D=$(date +%s)
    C=$(cat /proc/stat | awk '/cpu /{print $2":"$3":"$4":"$5}')
    N=$(cat /proc/net/dev | awk '/eth0/{print $2":"$10}')
    NW=$(cat /proc/net/dev | awk '/wlan0/{print $2":"$10}')
    NA=$(cat /proc/net/dev | awk '/ap0/{print $2":"$10}')
    rrdupdate temp.rrd ${D}:${T}
    rrdupdate cpu.rrd ${D}:${C}
    rrdupdate eth0.rrd ${D}:${N}
    rrdupdate wlan0.rrd ${D}:${NW}
    [ ! -z "$NA" ] && rrdupdate ap0.rrd ${D}:${NA}
    echo done.
    sleep 60
done

