
package main

import "fmt"
import "io"
import "path/filepath"
import "net/http"

func headers(w http.ResponseWriter, r *http.Request) {
    io.WriteString(w, "Headers:\n\n")
    for head, val := range r.Header {
        io.WriteString(w, fmt.Sprintf("  %s:\n", head))
        for i := range val {
            io.WriteString(w, fmt.Sprintf("    %s\n", val[i]))
        }
    }
}

func main() {
    fmt.Println("OK, listening..")
    http.HandleFunc("/headers", headers)

    fs := http.FileServer(http.Dir(filepath.FromSlash("./www")))
    // http.Handle("/www/", http.StripPrefix("/www", fs))
    http.Handle("/", fs)

    http.ListenAndServe(":8080", nil)
}

