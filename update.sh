#!/bin/bash

export GOPATH=~/go
GO=~/root/go/bin/go

cd ~
$GO get -u gitlab.com/galberti/sewes

UPD=update.sh
UPDN=go/src/gitlab.com/galberti/sewes/$UPD
UPDO=www/$UPD
MD5N=$(md5sum $UPDN | awk '{print $1}')
MD5O=$(md5sum $UPDO | awk '{print $1}')
if [ $MD5N != $MD5O ]
then
    cp $UPDN $UPDO
    cd www
    exec ./update.sh
fi

killall sewes
killall rec.sh
killall gengraph.sh
cp go/src/gitlab.com/galberti/sewes/rec.sh www/
cp go/src/gitlab.com/galberti/sewes/gengraph.sh www/
cp go/src/gitlab.com/galberti/sewes/start.sh www/
cp go/src/gitlab.com/galberti/sewes/genrrd.sh www/
rm -f www/www/*
cp go/src/gitlab.com/galberti/sewes/www/* www/www/
cd www
./genrrd.sh
./start.sh

